//
// C++ Interface: client
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef XMMSQT_CLIENT_H
#define XMMSQT_CLIENT_H

#include <QObject>
#include <QSocketNotifier>
#include <xmmsclient/xmmsclient.h>
#include "playback.h"
#include "playlist.h"

namespace XmmsQt {
	class QClient : public QObject {
		Q_OBJECT
	private:
		QString m_Name;
		bool m_Connected;
		xmmsc_connection_t * m_Connection;
		
		QSocketNotifier * m_ReadNotifier;
		QSocketNotifier * m_WriteNotifier;
		
/*		QPlayback m_Playback;
		QPlaylist m_Playlist;*/
		
		static void disconnectCallbackHandler(void * instance);
		static void outputCheck(int i, void * instance);
	private slots:
		void handleIn(int socket);
		void handleOut(int socket);
	public:
		QClient(QString name, QObject * parent = 0);
		~QClient();
		
/*		QPlayback * playback() { return &m_Playback; }
		QPlaylist * playlist() { return &m_Playlist; }*/
		
		void connectToServer(QString ipcPath = QString());
		bool isConnectedToServer() const;
		
		void quitServer();
		
		QString lastError() const;
		inline xmmsc_connection_t * connection() const { return m_Connection; }
		
// 		friend void disconnectCallbackHelper(void *);
	signals:
		void connectionChanged(xmmsc_connection_t * connection);
	};
}

#endif
