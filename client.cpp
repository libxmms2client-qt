//
// C++ Implementation: client
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#include "client.h"

namespace XmmsQt {
	QClient::QClient(QString name, QObject * parent) : QObject(parent),
	m_Name(name), m_Connected(false), m_Connection(NULL), m_ReadNotifier(NULL), m_WriteNotifier(NULL)
// 	m_Playback(this), m_Playlist(this)
	{
		m_Connection = xmmsc_init(name.toAscii().data());
	}
	
	QClient::~QClient() {
		if( m_Connection ) {
			xmmsc_unref(m_Connection);
		}
	}
	
	void QClient::disconnectCallbackHandler(void * instance) {
		if (instance) {
			QClient * client = static_cast<QClient *>(instance);
			xmmsc_unref(client->m_Connection);
			client->m_Connection = 0;
			
			if (client->m_ReadNotifier)
				delete client->m_ReadNotifier;
			if (client->m_WriteNotifier)
				delete client->m_WriteNotifier;
			
			emit client->connectionChanged(NULL); //TODO: check if this is possible
		}
	}
	
	void QClient::outputCheck(int, void * instance) {
		if (instance) {
			QClient * client = static_cast<QClient *>(instance);
			if (xmmsc_io_want_out(client->m_Connection))
				client->m_WriteNotifier->setEnabled(true);
			else
				client->m_WriteNotifier->setEnabled(false);
		}
	}
	
	void QClient::handleIn(int) {
		xmmsc_io_in_handle(m_Connection);
	}
	
	void QClient::handleOut(int) {
		xmmsc_io_out_handle(m_Connection);
	}
	
	void QClient::connectToServer(QString ipcPath) {
		if(!m_Connected) {
			if(!m_Connection) {
				m_Connection = xmmsc_init(m_Name.toAscii().data());
			}
			m_Connected = xmmsc_connect(m_Connection, (ipcPath.isEmpty()) ? NULL : ipcPath.toAscii().data());
		}
		if (!m_Connected)
			return;
		
		xmmsc_disconnect_callback_set(m_Connection, disconnectCallbackHandler, this);
		xmmsc_io_need_out_callback_set(m_Connection, outputCheck, this);
		
		//don't think we need a quit handler as we have a disconnect handler
		//xmmsc_result_notifier_set();
		
		//xmmsc_io_need_out_callback_set();
		
		if (m_ReadNotifier)
			delete m_ReadNotifier;
		if (m_WriteNotifier)
			delete m_WriteNotifier;
		
		m_ReadNotifier = new QSocketNotifier(xmmsc_io_fd_get(m_Connection), QSocketNotifier::Read, this);
		m_WriteNotifier = new QSocketNotifier(xmmsc_io_fd_get(m_Connection), QSocketNotifier::Write, this);
		connect(m_ReadNotifier, SIGNAL(activated(int)), this, SLOT(handleIn(int)));
		connect(m_WriteNotifier, SIGNAL(activated(int)), this, SLOT(handleOut(int)));
		m_ReadNotifier->setEnabled(true);
		m_WriteNotifier->setEnabled(false);
		
		emit connectionChanged(m_Connection);
	}
	
	bool QClient::isConnectedToServer() const
	{
		return m_Connected;
	}
	
	void QClient::quitServer() {
		if(m_Connected) {
			xmmsc_result_t * result = xmmsc_quit(m_Connection);
			xmmsc_result_unref(result);
			m_Connected = false;
		}
	}
	
	QString QClient::lastError() const {
		return QString(xmmsc_get_last_error(m_Connection));
	}
}
