//
// C++ Implementation: playback
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#include "common.h"
#include "playback.h"
#include "client.h"
#include <QDebug>

namespace XmmsQt {
	QPlayback::QPlayback(QClient * parent) : QObject(parent), m_Connection(0) {
		connect(parent, SIGNAL(connectionChanged(xmmsc_connection_t *)),
		        this,   SLOT(setConnection(xmmsc_connection_t *)));
		if (parent->isConnectedToServer())
			setConnection(parent->connection());
		else
			m_Connection = NULL;
	}
	
	QPlayback::~QPlayback() {
	}
	
	void QPlayback::setVolumeChannel(const void * key, xmmsc_result_value_type_t, const void * value, void * instance) {
		if (instance) {
			QPlayback * playback = static_cast<QPlayback *>(instance);
			QString hashKey((const char *)key);
			quint32 volume = XPOINTER_TO_INT(value);
			if (playback->m_Volume.contains(hashKey))
				playback->m_Volume[hashKey] = volume;
			else
				playback->m_Volume.insert(hashKey, volume);
		}
	}
	
	//callback for the "volume changed"-broadcast
	void QPlayback::volumeCallback(xmmsc_result_t * result, void * instance) {
		if (xmmsc_result_iserror(result)) {
			qDebug() << "Error: (QPlayback::volumeCallback)" << xmmsc_result_get_error(result);
			return;
		}
		
		if (instance) {
			QPlayback * playback = static_cast<QPlayback *>(instance);
			xmmsc_result_dict_foreach(result, setVolumeChannel, instance);
			emit playback->volumeChanged();
		}
		//xmmsc_result_unref(result);
	}
	
	//callback for the "status changed"-broadcast
	void QPlayback::statusCallback(xmmsc_result_t * result, void * instance) {
		if (xmmsc_result_iserror(result)) {
			qDebug() << "Error: (QPlayback::statusCallback)" << xmmsc_result_get_error(result);
			return;
		}
		
		if (instance) {
			QPlayback * playback = static_cast<QPlayback *>(instance);
			quint32 buffer;
			xmmsc_result_get_uint(result, &buffer);
			playback->m_Status = (xmms_playback_status_t)buffer;
			emit playback->statusChanged(playback->m_Status);
		}
		//xmmsc_result_unref(result);
	}
	
	//callback for the "id changed"-broadcast
	void QPlayback::idCallback(xmmsc_result_t * result, void * instance) {
		if (xmmsc_result_iserror(result)) {
			qDebug() << "Error: (QPlayback::idCallback)" << xmmsc_result_get_error(result);
			return;
		}
		
		if (instance) {
			QPlayback * playback = static_cast<QPlayback *>(instance);
			xmmsc_result_get_uint(result, &playback->m_CurrentID);
			emit playback->idChanged(playback->m_CurrentID);
		}
		//xmmsc_result_unref(result);
	}
	
	//callback for the "play time changed"-signal
	void QPlayback::playTimeCallback(xmmsc_result_t * result, void * instance) {
		if (xmmsc_result_iserror(result)) {
			qDebug() << "Error: (QPlayback::playTimeCallback)" << xmmsc_result_get_error(result);
			return;
		}
		
		if (instance) {
			QPlayback * playback = static_cast<QPlayback *>(instance);
			quint32 time;
			xmmsc_result_get_uint(result, &time);
			xmmsc_result_unref(xmmsc_result_restart(result));
			emit playback->playTimeChanged(time);
		}
		xmmsc_result_unref(result);
	}
	
	void QPlayback::reset() {
		if (m_Connection) {
			connectNotifierUnref(xmmsc_playback_volume_get(m_Connection), volumeCallback,   this);
			connectNotifierUnref(xmmsc_playback_current_id(m_Connection), idCallback,       this);
			connectNotifierUnref(xmmsc_playback_status(m_Connection),     statusCallback,   this);
		}
	}
	
	void QPlayback::setConnection(xmmsc_connection_t * connection) {
		if (m_Connection == connection)
			return;
		
		m_Volume.clear();
		
		//disconnect
		if (m_Connection) {
			disconnectNotifierUnref(xmmsc_broadcast_playback_volume_changed(m_Connection));
			disconnectNotifierUnref(xmmsc_broadcast_playback_current_id(m_Connection));
			disconnectNotifierUnref(xmmsc_broadcast_playback_status(m_Connection));
			disconnectNotifierUnref(xmmsc_signal_playback_playtime(m_Connection));
		}
		
		m_Connection = connection;
		m_CurrentID = 0;
		m_Status = XMMS_PLAYBACK_STATUS_STOP;
		
		//connect
		if (m_Connection) {
			connectNotifierUnref(xmmsc_broadcast_playback_volume_changed(m_Connection), volumeCallback,   this);
			connectNotifierUnref(xmmsc_broadcast_playback_current_id(m_Connection),     idCallback,       this);
			connectNotifierUnref(xmmsc_broadcast_playback_status(m_Connection),         statusCallback,   this);
			connectNotifierUnref(xmmsc_signal_playback_playtime(m_Connection),          playTimeCallback, this);
			
			connectNotifierUnref(xmmsc_playback_volume_get(m_Connection), volumeCallback,   this);
			connectNotifierUnref(xmmsc_playback_current_id(m_Connection), idCallback,       this);
			connectNotifierUnref(xmmsc_playback_status(m_Connection),     statusCallback,   this);
		}
		else {
			emit idChanged(0);
			emit volumeChanged();
			emit statusChanged(XMMS_PLAYBACK_STATUS_STOP);
		}
	}
	
	void QPlayback::start() {
		xmmsc_result_unref(xmmsc_playback_start(m_Connection));
	}
	
	void QPlayback::pause() {
		xmmsc_result_unref(xmmsc_playback_pause(m_Connection));
	}
	
	void QPlayback::stop() {
		xmmsc_result_unref(xmmsc_playback_stop(m_Connection));
	}
	
	void QPlayback::tickle() {
		xmmsc_result_unref(xmmsc_playback_tickle(m_Connection));
	}
	
	void QPlayback::setVolume(int volume, QString channel) {
		if (channel.isEmpty()) {
			foreach(QString i, m_Volume.keys())
				xmmsc_result_unref(xmmsc_playback_volume_set(m_Connection, i.toAscii().data(), volume));
		}
		else
			xmmsc_result_unref(xmmsc_playback_volume_set(m_Connection, channel.toAscii().data(), volume));
	}
	
	void QPlayback::setNext(quint32 position) {
		xmmsc_result_unref(xmmsc_playlist_set_next(m_Connection, position));
	}
	
	void QPlayback::setPlayTime(int time) {
		xmmsc_result_unref(xmmsc_playback_seek_ms(m_Connection, time));
	}
}
