//
// C++ Interface: xmmsclient-qt
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#include "xmmsclient-qt4/client.h"
#include "xmmsclient-qt4/playlist.h"
#include "xmmsclient-qt4/playback.h"
#include "xmmsclient-qt4/medialib.h"
