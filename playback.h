//
// C++ Interface: playback
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef XMMSQT_PLAYBACK_H
#define XMMSQT_PLAYBACK_H

#include <QObject>
#include <QHash>
#include <xmmsclient/xmmsclient.h>

namespace XmmsQt {
	class QClient;
	
	class QPlayback : public QObject {
		Q_OBJECT
	private:
		xmmsc_connection_t * m_Connection;
		QHash<QString, quint8> m_Volume;
		xmms_playback_status_t m_Status;
		quint32 m_CurrentID;
		
		static void setVolumeChannel(const void * key, xmmsc_result_value_type_t, const void * value, void * instance);
		
		static void volumeCallback(xmmsc_result_t * result, void * instance);
		static void statusCallback(xmmsc_result_t * result, void * instance);
		static void idCallback(xmmsc_result_t * result, void * instance);
		static void playTimeCallback(xmmsc_result_t * result, void * instance);
	public:
		QPlayback(QClient * parent);
		~QPlayback();
		
		xmms_playback_status_t status() { return m_Status; }
		quint32 currentID() const { return m_CurrentID; }
		const QHash<QString, quint8> & volume() { return m_Volume; }
		
		void reset();
	public slots:
		void setConnection(xmmsc_connection_t * connection);
		void start();
		void pause();
		void stop();
		void tickle();
		void setVolume(int volume, QString channel = QString());
		void setNext(quint32 position);
		void setPlayTime(int time);
	signals:
		void playTimeChanged(quint32 time);
		void volumeChanged();
		void statusChanged(xmms_playback_status_t status);
		void idChanged(quint32 id);
	};
}

#endif
