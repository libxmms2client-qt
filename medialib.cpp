//
// C++ Implementation: medialib
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#include "medialib.h"
#include "client.h"
#include "common.h"
#include <QDebug>
namespace XmmsQt {
	QMedialib::QMedialib(QClient * parent) : QObject(parent), m_Connection(0), m_ActivePlaylist(0) {
		connect(parent, SIGNAL(connectionChanged(xmmsc_connection_t *)),
		        this,   SLOT(setConnection(xmmsc_connection_t *)));
		if (parent->isConnectedToServer())
			setConnection(parent->connection());
		else
			m_Connection = NULL;
	}
	
	QMedialib::~QMedialib() {
		clearMediaInfo();
		foreach(QPlaylist * i, m_Playlists)
			delete i;
		m_Playlists.clear();
	}
	
	void QMedialib::playlistChangedCallback(xmmsc_result_t * result, void * instance) {
		if (xmmsc_result_iserror(result)) {
			qDebug() << "Error (QMedialib::playlistChangedCallback): " << xmmsc_result_get_error(result);
			return;
		}
		
		if (instance) {
			QMedialib * medialib = static_cast<QMedialib *>(instance);
			const char * strBuffer;
			xmmsc_result_get_dict_entry_string(result, "name", &strBuffer);
			QString name = strBuffer;
			QPlaylist::changedCallback(result, medialib->m_Playlists[name]);
		}
	}
	
	void QMedialib::currentPosCallback(xmmsc_result_t * result, void * instance) {
		if (xmmsc_result_iserror(result)) {
			qDebug() << "Error (QMedialib::currentPosCallback): " << xmmsc_result_get_error(result);
			return;
		}
		
		if (instance) {
			QMedialib * medialib = static_cast<QMedialib *>(instance);
			
			QPlaylist::currentPosCallback(result, medialib->m_ActivePlaylist);
		}
		//xmmsc_result_unref(result);
	}
	
	void QMedialib::activePlaylistCallback(xmmsc_result_t * result, void * instance) {
		if (xmmsc_result_iserror(result)) {
			qDebug() << "Error (QMedialib::activePlaylistCallback): " << xmmsc_result_get_error(result);
			return;
		}
		
		if (instance) {
			QMedialib * medialib = static_cast<QMedialib *>(instance);
			const char * strBuffer;
			xmmsc_result_get_string(result, &strBuffer);
			medialib->m_ActivePlaylist = medialib->m_Playlists[QString(strBuffer)];
			
			emit medialib->activePlaylistChanged();
		}
		//xmmsc_result_unref(result);
	}
	
	void QMedialib::fillPlaylists(xmmsc_result_t * result, void * instance) {
		if (xmmsc_result_iserror(result)) {
			qDebug() << "Error: (QMedialib::fillPlaylists)" << xmmsc_result_get_error(result);
			return;
		}
		
		if (instance) {
			QMedialib * medialib = static_cast<QMedialib *>(instance);
			
			const char * strBuffer;
			
			xmmsc_result_t * i = result;
			while (xmmsc_result_list_valid(i)) {
				xmmsc_result_get_string(i, &strBuffer);
				if (strBuffer[0] != '_')
					medialib->m_Playlists.insert(QString(strBuffer), new QPlaylist((QClient *)(medialib->parent()), QString(strBuffer)));
				xmmsc_result_list_next(i);
			}
			emit medialib->playlistsReseted();
		}
	}
	
	void QMedialib::collectionChangedCallback(xmmsc_result_t * result, void * instance) {
		if (xmmsc_result_iserror(result)) {
			qDebug() << "Error: (QMedialib::collectionChangedCallback)" << xmmsc_result_get_error(result);
			return;
		}
		
		if (instance) {
			QMedialib * medialib = static_cast<QMedialib *>(instance);
			
			qint32 type;
			xmmsc_result_get_dict_entry_int(result, "type", &type);
			
			const char * strBuffer;
			
			xmmsc_result_get_dict_entry_string(result, "namespace", &strBuffer);
			QString collNamespace = strBuffer;
			
			xmmsc_result_get_dict_entry_string(result, "name", &strBuffer);
			QString collName = strBuffer;
			
			//currently we only support playlists
			if (QString(collNamespace) != "Playlists")
				return;
			
			switch (type) {
			case XMMS_COLLECTION_CHANGED_ADD:
				medialib->m_Playlists.insert(collName, new QPlaylist((QClient *)(medialib->parent()), collName));
				emit medialib->collectionAdded(collName);
				break;
			case XMMS_COLLECTION_CHANGED_RENAME:
				xmmsc_result_get_dict_entry_string(result, "newname", &strBuffer);
				{
					QString collNewName = strBuffer;
					medialib->m_Playlists.insert(collNewName, medialib->m_Playlists.take(collName));
					
					emit medialib->collectionRenamed(collName, collNewName);
				}
				break;
			case XMMS_COLLECTION_CHANGED_UPDATE:
				emit medialib->collectionUpdated(collName);
				break;
			case XMMS_COLLECTION_CHANGED_REMOVE:
				if (medialib->m_Playlists.contains(collName)) {
					delete medialib->m_Playlists.take(collName);
					emit medialib->collectionRemoved(collName);
				}
				break;
			default:
				break;
			}
		}
	}
	
	void QMedialib::updatedMediaInfoCallback(xmmsc_result_t * result, void * instance) {
		if (xmmsc_result_iserror(result)) {
			qDebug() << "Error: (QMedialib::updatedMediaInfoCallback)" << xmmsc_result_get_error(result);
			return;
		}
		
		if (instance) {
			QMedialib * medialib = static_cast<QMedialib *>(instance);
			quint32 id;
			xmmsc_result_get_uint(result, &id);
			qDebug() << "Media info updated for: #" << id;
			if (medialib->checkMediaInfo(id)) {
				medialib->removeMediaInfo(id);
				medialib->requestMediaInfo(id);
			}
		}
	}
	
	void QMedialib::recievedMediaInfoCallback(xmmsc_result_t * result, void * instance) {
		if (xmmsc_result_iserror(result)) {
			qDebug() << "Error: (QMedialib::recievedMediaInfoCallback)" << xmmsc_result_get_error(result);
			return;
		}
		
		if (instance) {
			QMedialib * medialib = static_cast<QMedialib *>(instance);
			int intBuffer, id;
			const char * strBuffer;
			
			xmmsc_result_get_dict_entry_int(result, "id", &id);
			if (medialib->m_CachedMedia.contains(id)) {
				xmmsc_result_unref(result);
				return;
			}
			
			MediaInfo * newMediaInfo = new MediaInfo();
			
			if (resultHasKey(result, "genre")) {
				xmmsc_result_get_dict_entry_string(result, "genre",  &strBuffer);
				newMediaInfo->genre = strBuffer;
			}
			if (resultHasKey(result, "album")) {
				xmmsc_result_get_dict_entry_string(result, "album",  &strBuffer);
				newMediaInfo->album = strBuffer;
			}
			if (resultHasKey(result, "artist")) {
				xmmsc_result_get_dict_entry_string(result, "artist", &strBuffer);
				newMediaInfo->artist = strBuffer;
			}
			if (resultHasKey(result, "title")) {
				xmmsc_result_get_dict_entry_string(result, "title",  &strBuffer);
				newMediaInfo->title = strBuffer;
			}
			if (resultHasKey(result, "url")) {
				xmmsc_result_get_dict_entry_string(result, "url",  &strBuffer);
				newMediaInfo->url = strBuffer;
			}
			
			if (resultHasKey(result, "duration")) {
				if (xmmsc_result_get_dict_entry_int(result, "duration", &intBuffer))
					newMediaInfo->duration = intBuffer;
				else
					newMediaInfo->duration = 0;
			}
			
			if (resultHasKey(result, "channels")) {
				xmmsc_result_get_dict_entry_int(result, "channels", &intBuffer);
				newMediaInfo->channels = intBuffer;
			}
			if (resultHasKey(result, "samplerate")) {
				xmmsc_result_get_dict_entry_int(result, "samplerate", &intBuffer);
				newMediaInfo->sampleRate = intBuffer;
			}
			if (resultHasKey(result, "sample_format")) {
				xmmsc_result_get_dict_entry_int(result, "sample_format", &intBuffer);
				newMediaInfo->sampleFormat = intBuffer;
			}
			
			qDebug() << "Inserting media info #" << id << " with "
				<< newMediaInfo->artist << ", "
				<< newMediaInfo->title << ", "
				<< newMediaInfo->album << ", "
				<< newMediaInfo->duration;
			qDebug() << "-> url: " << newMediaInfo->url;
			medialib->m_CachedMedia.insert(id, newMediaInfo);
			emit medialib->mediaInfoReady(id);
		}
		xmmsc_result_unref(result);
	}
	
	void QMedialib::setConnection(xmmsc_connection_t * connection) {
		if (m_Connection == connection)
			return;
		
		//disconnect
		if (m_Connection) {
			disconnectNotifierUnref(xmmsc_broadcast_playlist_loaded(m_Connection));
			disconnectNotifierUnref(xmmsc_broadcast_playlist_changed(m_Connection));
			disconnectNotifierUnref(xmmsc_broadcast_playlist_current_pos(m_Connection));
			disconnectNotifierUnref(xmmsc_broadcast_collection_changed(m_Connection));
			disconnectNotifierUnref(xmmsc_broadcast_medialib_entry_changed(m_Connection));
		}
		
		m_Connection = connection;
		
		//connect
		if (m_Connection) {
			connectNotifierUnref(xmmsc_broadcast_playlist_loaded(m_Connection), activePlaylistCallback, this);
			connectNotifierUnref(xmmsc_broadcast_playlist_changed(m_Connection), playlistChangedCallback, this);
			connectNotifierUnref(xmmsc_broadcast_playlist_current_pos(m_Connection), currentPosCallback, this);
			connectNotifierUnref(xmmsc_broadcast_collection_changed(m_Connection), collectionChangedCallback, this);
			connectNotifierUnref(xmmsc_broadcast_medialib_entry_changed(m_Connection), updatedMediaInfoCallback, this);
			
			connectNotifierUnref(xmmsc_playlist_list(m_Connection), fillPlaylists, this);
			connectNotifierUnref(xmmsc_playlist_current_active(m_Connection), activePlaylistCallback, this);
		}
		
		clearMediaInfo();
	}
	
	void QMedialib::setActivePlaylist(QString name) {
		xmmsc_result_unref(xmmsc_playlist_load(m_Connection, name.toAscii().data()));
	}
	
	void QMedialib::clearMediaInfo() {
		foreach (MediaInfo * i, m_CachedMedia)
			delete i;
		m_CachedMedia.clear();
	}
	
	void QMedialib::removeMediaInfo(quint32 id) {
		delete m_CachedMedia.take(id);
	}
	
	bool QMedialib::checkMediaInfo(quint32 id) {
		if (m_CachedMedia.contains(id))
			return true;
		else {
			requestMediaInfo(id);
			return false;
		}
	}
	
	void QMedialib::requestMediaInfo(quint32 id) {
		connectNotifierUnref(xmmsc_medialib_get_info(m_Connection, id), recievedMediaInfoCallback, this);
	}
}
