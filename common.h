//
// C++ Interface: common
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//

#ifndef XMMSQT_COMMON_H
#define XMMSQT_COMMON_H
#include <xmmsclient/xmmsclient.h>

namespace XmmsQt {
	inline void connectNotifierUnref(xmmsc_result_t * result, xmmsc_result_notifier_t callback, void * userdata) {
		xmmsc_result_notifier_set(result, callback, userdata);
		xmmsc_result_unref(result);
	}
	
	inline void disconnectNotifierUnref(xmmsc_result_t * result) {
		xmmsc_result_disconnect(result);
		xmmsc_result_unref(result);
	}
	
	inline bool resultHasKey(xmmsc_result_t * result, const char * key) {
		return xmmsc_result_get_dict_entry_type(result, key) != XMMSC_RESULT_VALUE_TYPE_NONE;
	}
}
#endif
