
TEMPLATE = lib
CONFIG += static create_prl

CONFIG += qt warn_on exceptions dll
QT -= gui

CODECFORSRC = UTF-8


OBJECTS_DIR = build/
UI_DIR = build/
MOC_DIR = build/

target.path = /usr/lib/
INSTALLS += target includes maininclude
DESTDIR = .

INCLUDEPATH += /usr/include/xmms2/

HEADERS += client.h \
 playback.h \
 playlist.h \
 common.h \
 xmmsclient/xmmsclient-qt4.h \
 medialib.h

SOURCES += client.cpp \
 playback.cpp \
 playlist.cpp \
 medialib.cpp

LIBS += -lxmmsclient

VERSION = 1.0.0


includes.files += *.h
includes.path = /usr/include/xmms2/xmmsclient/xmmsclient-qt4/

maininclude.files += xmmsclient/xmmsclient-qt4.h
maininclude.path = /usr/include/xmms2/xmmsclient/
CONFIG -= thread

TARGET = xmmsclient-qt4

