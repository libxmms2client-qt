//
// C++ Implementation: playlist
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#include "common.h"
#include "playlist.h"
#include "client.h"
#include <QDir>
#include <QDebug>
namespace XmmsQt {
	QPlaylist::QPlaylist(QClient * parent, QString identifier) : QObject(parent), m_Identifier(identifier) {
		connect(parent, SIGNAL(connectionChanged(xmmsc_connection_t *)),
		        this,   SLOT(setConnection(xmmsc_connection_t *)));
		if (parent->isConnectedToServer())
			setConnection(parent->connection());
		else
			m_Connection = NULL;
	}
	
	QPlaylist::~QPlaylist() {
	}
	
	void QPlaylist::changedCallback(xmmsc_result_t * result, void * instance) {
		if (xmmsc_result_iserror(result)) {
			qDebug() << "Error (QPlaylist::changedCallback): " << xmmsc_result_get_error(result);
			return;
		}
		
		if (instance) {
			QPlaylist * playlist = static_cast<QPlaylist *>(instance);
			
			qint32 type;
			xmmsc_result_get_dict_entry_int(result, "type", &type);
			
			qDebug() << "Playlist " << playlist->m_Identifier << " has changed (" << type << ')';
			
			switch (type) {
			case XMMS_PLAYLIST_CHANGED_ADD:
				{
					quint32 id;
					
					xmmsc_result_get_dict_entry_uint(result, "id", &id);
					qDebug() << "Adding #" << id;
					emit playlist->entryAboutToBeInserted(playlist->m_IDList.size(), id);
					playlist->m_IDList << id;
					emit playlist->entryInserted(playlist->m_IDList.size());
				}
				break;
			case XMMS_PLAYLIST_CHANGED_INSERT:
				{
					quint32 id;
					qint32 position;
					
					xmmsc_result_get_dict_entry_int(result, "position", &position);
					xmmsc_result_get_dict_entry_uint(result, "id", &id);
					qDebug() << "Inserting #" << id << " at " << position;
					emit playlist->entryAboutToBeInserted(position, id);
					playlist->m_IDList.insert(position, id);
					emit playlist->entryInserted(position);
				}
				break;
			case XMMS_PLAYLIST_CHANGED_UPDATE:
				{
					quint32 oldid;
					qint32 position;
					
					xmmsc_result_get_dict_entry_int(result, "position", &position);
					oldid = playlist->m_IDList[position];
					xmmsc_result_get_dict_entry_uint(result, "id", &playlist->m_IDList[position]);
					qDebug() << "Updating " << position << " form #" << oldid << " to #" << playlist->m_IDList[position];
					emit playlist->entryUpdated(position, oldid);
				}
				break;
			case XMMS_PLAYLIST_CHANGED_REMOVE:
				{
					quint32 id;
					qint32 position;
					
					xmmsc_result_get_dict_entry_int(result, "position", &position);
					id = playlist->m_IDList[position];
					qDebug() << "Removing #" << id << " at " << position;
					emit playlist->entryAboutToBeRemoved(position, id);
					playlist->m_IDList.removeAt(position);
					emit playlist->entryRemoved(position);
				}
				break;
			case XMMS_PLAYLIST_CHANGED_MOVE:
				{
					quint32 from, to;
					
					xmmsc_result_get_dict_entry_uint(result, "position", &from);
					xmmsc_result_get_dict_entry_uint(result, "newposition", &to);
					playlist->m_IDList.move(from, to);
					qDebug() << "Moving " << from << "to" << to;
					emit playlist->entryMoved(from, to);
				}
				break;
			case XMMS_PLAYLIST_CHANGED_SORT:
			case XMMS_PLAYLIST_CHANGED_SHUFFLE:
			case XMMS_PLAYLIST_CHANGED_CLEAR:
				qDebug() << "Reseting";
				playlist->reset();
				break;
			default:
				break;
			}
		}
		//xmmsc_result_unref(result);
	}
	
	void QPlaylist::fillIDListCallback(xmmsc_result_t * result, void * instance) {
		if (xmmsc_result_iserror(result)) {
			qDebug() << "Error (QPlaylist::fillIDListCallback): " << xmmsc_result_get_error(result);
			return;
		}
		
		if (instance) {
			QPlaylist * playlist = static_cast<QPlaylist *>(instance);
			
			quint32 buffer;
			
			xmmsc_result_t * i = result;
			while (xmmsc_result_list_valid(i)) {
				xmmsc_result_get_uint(i, &buffer);
				playlist->m_IDList << buffer;
				xmmsc_result_list_next(i);
			}
			emit playlist->reseted();
		}
		//xmmsc_result_unref(result);
	}
	
	void QPlaylist::currentPosCallback(xmmsc_result_t * result, void * instance) {
		if (xmmsc_result_iserror(result)) {
			qDebug() << "Error (QPlaylist::currentPosCallback): " << xmmsc_result_get_error(result);
			return;
		}
		
		if (instance) {
			QPlaylist * playlist = static_cast<QPlaylist *>(instance);
			xmmsc_result_get_uint(result, &playlist->m_Position);
			emit playlist->positionChanged(playlist->m_Position);
		}
		//xmmsc_result_unref(result);
	}
	
	inline const char * QPlaylist::identifierToCString() {
		return m_Identifier.isEmpty() ? NULL : m_Identifier.toAscii().data();
	}
	
	void QPlaylist::setConnection(xmmsc_connection_t * connection) {
		if (m_Connection == connection)
			return;
		
		//disconnect
/*		if (m_Connection) {
			disconnectNotifierUnref(xmmsc_broadcast_playlist_loaded(m_Connection));
			disconnectNotifierUnref(xmmsc_broadcast_playlist_changed(m_Connection));
			disconnectNotifierUnref(xmmsc_broadcast_playlist_current_pos(m_Connection));
		}*/
		
		m_Connection = connection;
		
		m_IDList.clear();
		m_Position = 0;
		
		//connect
		if (m_Connection) {
/*			connectNotifierUnref(xmmsc_broadcast_playlist_loaded(m_Connection),      loadedCallback,     this);
			connectNotifierUnref(xmmsc_broadcast_playlist_changed(m_Connection),     changedCallback,    this);
			connectNotifierUnref(xmmsc_broadcast_playlist_current_pos(m_Connection), currentPosCallback, this);*/
			
			connectNotifierUnref(xmmsc_playlist_list_entries(m_Connection, identifierToCString()), fillIDListCallback, this);
			connectNotifierUnref(xmmsc_playlist_current_pos(m_Connection, identifierToCString()), currentPosCallback, this);
		}
		else {
			emit reseted();
			emit positionChanged(0);
		}
	}
	
	void QPlaylist::reset() {
		m_IDList.clear();
		emit reseted();
		if (m_Connection) {
			connectNotifierUnref(xmmsc_playlist_list_entries(m_Connection, identifierToCString()), fillIDListCallback, this);
			connectNotifierUnref(xmmsc_playlist_current_pos(m_Connection, identifierToCString()), currentPosCallback, this);
		}
		
	}
	
	void QPlaylist::append(quint32 id) {
		xmmsc_result_unref(xmmsc_playlist_add_id(m_Connection, identifierToCString(), id));
	}
	
	void QPlaylist::append(QString url) {
		xmmsc_result_unref(xmmsc_playlist_add_url(m_Connection, identifierToCString(), url.toAscii().data()));
	}
	
	void QPlaylist::append(QStringList urls) {
		foreach(QString i, urls)
			append(i);
	}
	
	void QPlaylist::append(QList<quint32> ids) {
		foreach(quint32 i, ids)
			append(i);
	}
	
	void QPlaylist::appendRecursive(QString directory) {
		xmmsc_result_unref(xmmsc_playlist_radd(m_Connection, identifierToCString(), directory.toAscii().data()));
	}
	
	void QPlaylist::insert(quint32 id, quint32 position) {
		xmmsc_result_unref(xmmsc_playlist_insert_id(m_Connection, identifierToCString(), position, id));
	}
	
	void QPlaylist::insert(QString url, quint32 position) {
		xmmsc_result_unref(xmmsc_playlist_insert_url(m_Connection, identifierToCString(), position, url.toAscii().data()));
	}
	
	void QPlaylist::insert(QList<quint32> ids, quint32 position) {
		if (ids.count() == 0)
			return;
		
		QList<quint32>::iterator i = ids.end();
		do {
			i--;
			insert(*i, position);
		}
		while (i != ids.begin());
	}
	
	void QPlaylist::insert(QStringList urls, quint32 position) {
		if (urls.count() == 0)
			return;
		
		QStringList::iterator i = urls.end();
		do {
			i--;
			insert(*i, position);
		}
		while (i != urls.begin());
	}
	
	void QPlaylist::remove(quint32 position) {
		xmmsc_result_unref(xmmsc_playlist_remove_entry(m_Connection, identifierToCString(), position));
	}
	
	void QPlaylist::move(quint32 from, quint32 to) {
		xmmsc_result_unref(xmmsc_playlist_move_entry(m_Connection, identifierToCString(), from, to));
	}
	
	void QPlaylist::clear() {
		xmmsc_result_unref(xmmsc_playlist_clear(m_Connection, identifierToCString()));
	}
	
	void QPlaylist::sort(QStringList properties) {
		const char ** prop = new const char *[properties.count()+1];
		prop[properties.count()] = NULL;
		
		for (int i = 0; i < properties.count(); i++)
			prop[i] = properties[i].toAscii().data();
		
		xmmsc_result_unref(xmmsc_playlist_sort(m_Connection, "Default", prop));
		
		delete [] prop;
	}
	
	void QPlaylist::randomize() {
		xmmsc_result_unref(xmmsc_playlist_shuffle(m_Connection, identifierToCString()));
	}
}
