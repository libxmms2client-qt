//
// C++ Interface: medialib
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef XMMSQT_MEDIALIB_H
#define XMMSQT_MEDIALIB_H

#include <QObject>
#include <QHash>
#include <xmmsclient/xmmsclient.h>

namespace XmmsQt {
	class QClient;
	class QPlaylist;
	
	struct MediaInfo {
		quint16 tracknr;
		QString title;
		QString artist;
		QString album;
		QString genre;
		//QString comment;
		QString url;
		quint32 channels;
		quint32 sampleRate;
		quint32 sampleFormat;
		quint32 duration;
	};
	
	typedef QHash<quint32, MediaInfo *> QMediaInfoHash;
	typedef QHash<QString, QPlaylist *> QPlaylistHash;
	
	class QMedialib : public QObject {
		Q_OBJECT
	private:
		xmmsc_connection_t * m_Connection;
		QMediaInfoHash m_CachedMedia;
		QPlaylistHash m_Playlists;
		QPlaylist * m_ActivePlaylist;
		
		static void recievedMediaInfoCallback(xmmsc_result_t * result, void * instance);
		static void updatedMediaInfoCallback(xmmsc_result_t * result, void * instance);
		
		static void playlistChangedCallback(xmmsc_result_t * result, void * instance);
		static void activePlaylistCallback(xmmsc_result_t * result, void * instance);
		static void currentPosCallback(xmmsc_result_t * result, void * instance);
		static void collectionChangedCallback(xmmsc_result_t * result, void * instance);
		static void fillPlaylists(xmmsc_result_t *, void * instance);
	public slots:
		void setActivePlaylist(QString name);
	private slots:
		void setConnection(xmmsc_connection_t * connection);
		//void setActivePlaylist(QPlaylist * playlist);
	public:
		QMedialib(QClient * parent);
		~QMedialib();
		
		const QMediaInfoHash & media() { return m_CachedMedia; }
		const QPlaylistHash & playlists() { return m_Playlists; }
		QPlaylist * activePlaylist() const { return m_ActivePlaylist; };
		
		void clearMediaInfo();
		void removeMediaInfo(quint32 id);
		void requestMediaInfo(quint32 id);
		bool checkMediaInfo(quint32 id);
		//void resetPlaylists();
	signals:
		void mediaInfoReady(quint32 id);
		void activePlaylistChanged();
		void collectionAdded(QString name);
		void collectionRemoved(QString name);
		void collectionUpdated(QString name);
		void collectionRenamed(QString oldName, QString newName);
		void playlistsReseted();
	};
}

#endif
