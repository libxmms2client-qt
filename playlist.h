//
// C++ Interface: playlist
//
// Author: Oliver Groß <z.o.gross@gmx.de>, (C) 2008
//
// Copyright: See COPYING file that comes with this distribution
//
#ifndef XMMSQT_PLAYLIST_H
#define XMMSQT_PLAYLIST_H

#include <QObject>
#include <QStringList>
#include <xmmsclient/xmmsclient.h>

namespace XmmsQt {
	class QClient;
	class QMedialib;
	
	class QPlaylist : public QObject {
		Q_OBJECT
	friend class QMedialib;
	private:
		QString m_Identifier;
		xmmsc_connection_t * m_Connection;
		QList<quint32> m_IDList;
		quint32 m_Position;
		
		static void fillIDListCallback(xmmsc_result_t * result, void * instance);
		static void changedCallback(xmmsc_result_t * result, void * instance);
		static void currentPosCallback(xmmsc_result_t * result, void * instance);
		
		inline const char * identifierToCString();
	private slots:
		void setConnection(xmmsc_connection_t * connection);
	public:
		QPlaylist(QClient * parent, QString identifier = QString());
		~QPlaylist();
		
		xmmsc_connection_t * connection() const { return m_Connection; }
		QList<quint32> & idList() { return m_IDList; }
		quint32 position() const { return m_Position; }
		QString identifier() const { return m_Identifier; }
		
		void reset();
	public slots:
		void append(quint32 id);
		void append(QString url);
		void append(QList<quint32> ids);
		void append(QStringList urls);
		void appendRecursive(QString directory);
		void insert(quint32 id, quint32 position);
		void insert(QString url, quint32 position);
		void insert(QList<quint32> ids, quint32 position);
		void insert(QStringList urls, quint32 position);
		void remove(quint32 position);
		void move(quint32 from, quint32 to);
		void clear();
		void sort(QStringList properties);
		void randomize();
	signals:
		void entryAboutToBeInserted(quint32 position, quint32 id);
		void entryAboutToBeRemoved(quint32 position, quint32 id);
		void entryInserted(quint32 position);
		void entryRemoved(quint32 position);
		void entryMoved(quint32 from, quint32 to);
		void entryUpdated(quint32 position, quint32 oldid);
		void reseted();
		void positionChanged(quint32 position);
	};
}

#endif
